package com.petgroom.demo.repositories;

import com.petgroom.demo.models.CardInfo;
import org.springframework.data.repository.CrudRepository;

public interface CardInfoRepo extends CrudRepository<CardInfo, Integer> {
}

