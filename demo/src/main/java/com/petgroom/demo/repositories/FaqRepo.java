package com.petgroom.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petgroom.demo.models.Faq;

public interface FaqRepo extends JpaRepository<Faq, Integer> {
  List<Faq> findAllByOrderByCreatedAt();
}
