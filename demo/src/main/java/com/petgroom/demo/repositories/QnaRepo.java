package com.petgroom.demo.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petgroom.demo.models.Qna;

public interface QnaRepo extends JpaRepository<Qna, Integer> {
  List<Qna> findAllByOrderByCreatedAt();
}
