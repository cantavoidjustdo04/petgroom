package com.petgroom.demo.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.petgroom.demo.models.GroomingService;

public interface GroomingServiceRepo extends JpaRepository<GroomingService, Integer> {
  // 필요한 쿼리 메소드를 추가합니다.
}
