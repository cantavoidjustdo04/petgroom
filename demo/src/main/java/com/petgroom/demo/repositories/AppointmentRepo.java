package com.petgroom.demo.repositories;

import java.util.List;

// AppointmentRepository.java
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.petgroom.demo.models.Appointment;


public interface AppointmentRepo extends JpaRepository<Appointment, Integer> {
    // Additional custom query methods can be added here
    List<Appointment> findByUserEmail(String email);

    @Query(value = "SELECT COALESCE(SUM(price), 0) FROM appointment WHERE YEAR(appointment_date) = ?1 AND MONTH(appointment_date) = ?2", nativeQuery = true)
    int findTotalRevenueByMonth(int year, int month);
}

