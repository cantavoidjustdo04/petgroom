package com.petgroom.demo.repositories;

import com.petgroom.demo.models.User;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepo extends JpaRepository<User, Integer> {
  Optional<User> findByEmail(String email);
  int countByGender(String gender);
}
