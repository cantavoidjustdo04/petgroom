package com.petgroom.demo.repositories;

import com.petgroom.demo.models.Groomer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface GroomerRepo extends JpaRepository<Groomer, Integer> {
}

