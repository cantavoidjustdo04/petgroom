package com.petgroom.demo.models;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrePersist;
import javax.persistence.PreUpdate;

@Entity
public class Qna {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne(fetch = FetchType.LAZY)
  @JoinColumn(name = "user_id")
  private User user;

  @Column(name = "question_title")
  private String question_title;

  @Column(name = "question_content")
  private String question_content;

  @Column(name = "answer_content")
  private String answer_content;

  @Column(name = "created_at")
  private LocalDateTime createdAt;

  @Column(name = "updated_at")
  private LocalDateTime updatedAt;

  public Qna() {
  }

  

  public Qna(int id, User user, String question_title, String question_content, String answer_content,
      LocalDateTime createdAt, LocalDateTime updatedAt) {
    this.id = id;
    this.user = user;
    this.question_title = question_title;
    this.question_content = question_content;
    this.answer_content = answer_content;
    this.createdAt = createdAt;
    this.updatedAt = updatedAt;
  }



  @PrePersist
  protected void onCreate() {
    createdAt = LocalDateTime.now();
  }

  @PreUpdate
  protected void onUpdate() {
    updatedAt = LocalDateTime.now();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  
  public String getQuestion_title() {
    return question_title;
  }

  public void setQuestion_title(String question_title) {
    this.question_title = question_title;
  }

  public String getQuestion_content() {
    return question_content;
  }

  public void setQuestion_content(String question_content) {
    this.question_content = question_content;
  }

  public String getAnswer_content() {
    return answer_content;
  }

  public void setAnswer_content(String answer_content) {
    this.answer_content = answer_content;
  }

  public LocalDateTime getCreatedAt() {
    return createdAt;
  }

  public void setCreatedAt(LocalDateTime createdAt) {
    this.createdAt = createdAt;
  }

  public LocalDateTime getUpdatedAt() {
    return updatedAt;
  }

  public void setUpdatedAt(LocalDateTime updatedAt) {
    this.updatedAt = updatedAt;
  }



  public User getUser() {
    return user;
  }



  public void setUser(User user) {
    this.user = user;
  }

  // getters and setters

}
