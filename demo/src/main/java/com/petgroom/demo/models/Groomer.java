package com.petgroom.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class Groomer {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  @Column(name = "groomer_id")
  private int id;

  private String name;
  private String gender;
  private String phone;
  private String email;
  private String image;
  private int ranks;

  public Groomer() {
  }

  public Groomer(int id, String name, String gender, String phone, String email, String image, int ranks) {
    this.id = id;
    this.name = name;
    this.gender = gender;
    this.phone = phone;
    this.email = email;
    this.image = image;
    this.ranks = ranks;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public String getImage() {
    return image;
  }

  public void setImage(String image) {
    this.image = image;
  }

  public int getRanks() {
    return ranks;
  }

  public void setRanks(int ranks) {
    this.ranks = ranks;
  }


}
