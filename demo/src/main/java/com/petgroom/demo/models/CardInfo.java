package com.petgroom.demo.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Entity
public class CardInfo {

  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @Column(name = "card_number")
  private String cardnumber;

  @Column(name = "card_holder")
  private String cardholder;

  @Column(name = "expiry_date")
  private String expirydate;

  @Column(name = "cvv")
  private String cvv;

  public CardInfo(){
  }

  public CardInfo(int id, String cardnumber, String cardholder, String expirydate, String cvv) {
    this.id = id;
    this.cardnumber = cardnumber;
    this.cardholder = cardholder;
    this.expirydate = expirydate;
    this.cvv = cvv;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getCardnumber() {
    return cardnumber;
  }

  public void setCardnumber(String cardnumber) {
    this.cardnumber = cardnumber;
  }

  public String getCardholder() {
    return cardholder;
  }

  public void setCardholder(String cardholder) {
    this.cardholder = cardholder;
  }

  public String getExpirydate() {
    return expirydate;
  }

  public void setExpirydate(String expirydate) {
    this.expirydate = expirydate;
  }

  public String getCvv() {
    return cvv;
  }

  public void setCvv(String cvv) {
    this.cvv = cvv;
  }
}
