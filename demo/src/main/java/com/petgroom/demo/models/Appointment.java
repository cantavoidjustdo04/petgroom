package com.petgroom.demo.models;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Transient;

@javax.persistence.Entity
public class Appointment {
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;

  @ManyToOne
  @JoinColumn(name = "groomer_id", nullable = false)
  private Groomer groomer;
  @Column(name = "pet_type")
  private String petType;
  @Column(name = "pet_size")
  private String petSize;
  @Column(name = "appointment_date")
  private LocalDateTime appointmentDate;
  @Column(name = "payment_method")
  private String paymentMethod;

  @Transient
  private String appointmentDateStr;

  @OneToOne(cascade = CascadeType.ALL)
  @JoinColumn(name = "card_info_id")
  private CardInfo cardInfo;
  private double price;

  @ManyToOne
  @JoinColumn(name = "user_id", nullable = false)
  private User user;

  @ManyToMany
  @JoinTable(name = "appointment_grooming_service", joinColumns = @JoinColumn(name = "appointment_id"), inverseJoinColumns = @JoinColumn(name = "grooming_service_id"))
  private List<GroomingService> groomingServices = new ArrayList<>();

  public Appointment() {
  }

  // Offline Payment Constructor
  public Appointment(int id, User user, Groomer groomer, String petType, String petSize,
      LocalDateTime appointmentDate, String paymentMethod, double price) {
    this.id = id;
    this.user = user;
    this.groomer = groomer;
    this.petType = petType;
    this.petSize = petSize;
    this.appointmentDate = appointmentDate;
    this.paymentMethod = paymentMethod;
    this.price = price;
  }

  // Online Payment Constructor
  public Appointment(int id, User user, Groomer groomer, String petType, String petSize,
      LocalDateTime appointmentDate, String paymentMethod, CardInfo cardInfo, double price) {
    this.id = id;
    this.user = user;
    this.groomer = groomer;
    this.petType = petType;
    this.petSize = petSize;
    this.appointmentDate = appointmentDate;
    this.paymentMethod = paymentMethod;
    this.cardInfo = cardInfo;
    this.price = price;
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public Groomer getGroomer() {
    return groomer;
  }

  public void setGroomer(Groomer groomer) {
    this.groomer = groomer;
  }

  public String getPetType() {
    return petType;
  }

  public void setPetType(String petType) {
    this.petType = petType;
  }

  public String getPetSize() {
    return petSize;
  }

  public void setPetSize(String petSize) {
    this.petSize = petSize;
  }

  public LocalDateTime getAppointmentDate() {
    return appointmentDate;
  }

  public void setAppointmentDate(LocalDateTime appointmentDate) {
    this.appointmentDate = appointmentDate;
  }

  public String getPaymentMethod() {
    return paymentMethod;
  }

  public void setPaymentMethod(String paymentMethod) {
    this.paymentMethod = paymentMethod;
  }

  public CardInfo getCardInfo() {
    return cardInfo;
  }

  public void setCardInfo(CardInfo cardInfo) {
    this.cardInfo = cardInfo;
  }

  public double getPrice() {
    return price;
  }

  public void setPrice(double price) {
    this.price = price;
  }

  public String getAppointmentDateStr() {
    return appointmentDateStr;
  }

  public void setAppointmentDateStr(String appointmentDateStr) {
    this.appointmentDateStr = appointmentDateStr;
  }

  public User getUser() {
    return user;
  }

  public void setUser(User user) {
    this.user = user;
  }

  public List<GroomingService> getGroomingServices() {
    return groomingServices;
  }

  public void setGroomingServices(List<GroomingService> groomingServices) {
    this.groomingServices = groomingServices;
  }

}
