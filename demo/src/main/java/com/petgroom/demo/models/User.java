package com.petgroom.demo.models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@javax.persistence.Entity
public class User {
  
  @Id
  @GeneratedValue(strategy = GenerationType.IDENTITY)
  private int id;
  private String name;
  private String nickname;
  private String password;
  private String gender;
  private String phone;
  private String email;
  // "USER", "ADMIN"
  private String role;

  @Column(name="active")
  private boolean active;

  @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
  private List<Qna> qnas;
  
  public User(){
  }
  //Customer Constructor
  public User(int id, String name, String nickname, String password, String gender, String phone, String email,
      boolean active) {
    this.id = id;
    this.name = name;
    this.nickname = nickname;
    this.password = password;
    this.gender = gender;
    this.phone = phone;
    this.email = email;
    this.active = active;
    this.qnas = new ArrayList<>();
  }

  public int getId() {
    return id;
  }

  public void setId(int id) {
    this.id = id;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getNickname() {
    return nickname;
  }

  public void setNickname(String nickname) {
    this.nickname = nickname;
  }

  public String getPassword() {
    return password;
  }

  public void setPassword(String password) {
    this.password = password;
  }

  public String getGender() {
    return gender;
  }

  public void setGender(String gender) {
    this.gender = gender;
  }

  public String getPhone() {
    return phone;
  }

  public void setPhone(String phone) {
    this.phone = phone;
  }

  public String getEmail() {
    return email;
  }

  public void setEmail(String email) {
    this.email = email;
  }

  public boolean isActive() {
    return active;
  }

  public void setActive(boolean active) {
    this.active = active;
  }

  public String getRole() {
    return role;
  }

  public void setRole(String role) {
    this.role = role;
  }

  public List<Qna> getQnas() {
    return qnas;
  }

  public void setQnas(List<Qna> qnas) {
    this.qnas = qnas;
  }
}
