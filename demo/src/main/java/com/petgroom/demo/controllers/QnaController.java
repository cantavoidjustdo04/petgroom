package com.petgroom.demo.controllers;

import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

import com.petgroom.demo.models.Qna;
import com.petgroom.demo.models.User;
import com.petgroom.demo.repositories.UserRepo;
import com.petgroom.demo.services.QnaServices;

@Controller
@RequestMapping("/qna")
public class QnaController {
  @Autowired
  private QnaServices qnaService;

  @Autowired
  private UserRepo userRepo;

  public QnaController(QnaServices qnaService) {
    this.qnaService = qnaService;
  }

  @GetMapping
  public String showQnaPage(Model model) {
    List<Qna> qnas = qnaService.getAllQAs();
    if (!qnas.isEmpty()) {
      model.addAttribute("qnas", qnas);
    } else {
      System.out.println("is empty");
    }
    return "qna";
  }

  @GetMapping("/questionform")
  public String showQuestionForm(Model model) {
    model.addAttribute("qna", new Qna());
    return "questionform";
  }

  @GetMapping("/{id}/questiondetails")
  public String viewQuestion(@PathVariable Integer id, Model model, Principal principal) {
    Optional<Qna> qnaOptional = qnaService.findById(id);
    if (!qnaOptional.isPresent()) {
      return "redirect:/qna";
    }
    Qna qna = qnaOptional.get();
    model.addAttribute("qna", qna);
    // 관리자 확인
    boolean isAdmin = principal != null
        && userRepo.findByEmail(principal.getName()).map(User::getRole).orElse("").equalsIgnoreCase("admin");
    model.addAttribute("isAdmin", isAdmin);
    return "questiondetails";
  }


  @PostMapping("/{id}/answer")
  public String saveAnswer(@PathVariable Integer id, @RequestParam("answer_content") String answerContent) {
    Optional<Qna> qnaOptional = qnaService.findById(id);
    if (!qnaOptional.isPresent()) {
      return "redirect:/qna";
    }
    qnaOptional.get().setAnswer_content(answerContent);
    qnaService.save(qnaOptional.get());
    return "redirect:/qna";
  }

  @PostMapping("/questionform")
  public String saveQuestion(@Valid @ModelAttribute("qna") Qna qna, BindingResult bindingResult, Principal principal) {
    if (bindingResult.hasErrors()) {
      return "questionform";
    }
    User user = userRepo.findByEmail(principal.getName()).orElse(null);
    if (user == null) {
      System.out.println("No User");
    }
    qna.setUser(user);
    qnaService.save(qna);
    return "redirect:/qna";
  }

}
