package com.petgroom.demo.controllers;

import com.petgroom.demo.models.User;
import com.petgroom.demo.repositories.AppointmentRepo;
import com.petgroom.demo.models.Appointment;
import com.petgroom.demo.models.Groomer;
import com.petgroom.demo.services.AppointmentServices;
import com.petgroom.demo.services.GroomerServices;
import com.petgroom.demo.services.UserServices;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

@Controller
@RequestMapping("/")
public class UserController {

    @Autowired
    private UserServices userService;

    @Autowired
    private GroomerServices groomerService;

    @Autowired
    private AppointmentServices appointmentService;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private AppointmentRepo appointmentRepo;

    @GetMapping("/login")
    public String showLogin() {
        return "login.html";
    }

    @GetMapping("/register")
    public String showRegister(Model model) {
        model.addAttribute("user", new User());
        return "register.html";
    }

    @GetMapping("/admin")
    public String showAdmin(Model model) {
        List<Groomer> groomers = groomerService.getAllGroomers();
        List<Appointment> appointments = appointmentService.getAllAppointments();
    
        List<Integer> monthlyRevenueData = appointmentService.getMonthlyRevenueData();
        Map<String, Integer> genderRatioData = userService.getGenderRatioData();
    
        model.addAttribute("groomers", groomers);
        model.addAttribute("appointments", appointments);
        model.addAttribute("monthlyRevenueData", monthlyRevenueData);
        model.addAttribute("genderRatioData", genderRatioData);
        
        return "admin";
    }

    @GetMapping("/mypage")
    public String showMyPage(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName(); // 현재 로그인한 사용자의 이메일
        User user = userService.getUser(email); // 현재 사용자 정보 가져오기
        model.addAttribute("user", user); // 모델에 사용자 정보 추가
        return "mypage"; // Thymeleaf 템플릿 파일명 리턴
    }

    @GetMapping("/myAppointment")
    public String showMyAppointment(Model model) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        String email = auth.getName(); // 현재 로그인한 사용자의 이메일
        List<Appointment> appointmentList = appointmentRepo.findByUserEmail(email); // 현재 사용자의 예약 목록 조회

        LocalDate today = LocalDate.now();
        List<Appointment> pastAppointments = new ArrayList<>();
        List<Appointment> currentAppointments = new ArrayList<>();

        for (Appointment appointment : appointmentList) {
            LocalDate appointmentDate = appointment.getAppointmentDate().toLocalDate();
            if (appointmentDate.isBefore(today)) {
                pastAppointments.add(appointment);
            } else {
                currentAppointments.add(appointment);
            }
        }
        model.addAttribute("pastAppointments", pastAppointments); // 과거 예약 목록
        model.addAttribute("currentAppointments", currentAppointments); // 현재 예약 목록

        return "myAppointment"; // Thymeleaf 템플릿 파일명 리턴
    }

    @GetMapping("/cancel/{id}")
    public String cancelAppointment(@PathVariable("id") int id) {
        // Cancel the appointment using the repository
        appointmentRepo.deleteById(id);

        // Redirect the user back to their appointments
        return "redirect:/myAppointment";
    }

    @PostMapping("/register")
    public String newUser(@Valid @ModelAttribute("user") User user, BindingResult result) {
        if (result.hasErrors()) {
            return "register";
        }
        user.setRole("USER"); // default role = "USER"
        user.setActive(true);
        user.setPassword(passwordEncoder.encode(user.getPassword())); // encode the password with BCrypt
        userService.newUser(user);

        return "index";
    }

}
