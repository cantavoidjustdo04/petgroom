package com.petgroom.demo.controllers;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import com.petgroom.demo.models.Groomer;
import com.petgroom.demo.services.GroomerServices;

@Controller
@RequestMapping("/addgroomer")
public class GroomerController {
  
    @Autowired
    private GroomerServices groomerService;
    // @Autowired
    // private GroomerServices groomerService;

    @GetMapping
    public String showAddgroomer(Model model) {
        model.addAttribute("groomer", new Groomer());
        return "addgroomer";
    }

    @PostMapping
    public String newUser(@Valid @ModelAttribute("groomer") Groomer groomer, BindingResult result) {
        if (result.hasErrors()) {
            return "admin";
        }
        groomerService.saveGroomer(groomer);

        return "admin";
    }
}
