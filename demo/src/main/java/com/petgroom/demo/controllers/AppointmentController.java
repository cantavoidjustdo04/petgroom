package com.petgroom.demo.controllers;

import com.petgroom.demo.models.Appointment;
import com.petgroom.demo.models.Groomer;
import com.petgroom.demo.models.GroomingService;
import com.petgroom.demo.repositories.AppointmentRepo;
import com.petgroom.demo.services.GroomerServices;
import com.petgroom.demo.services.GroomingServiceServices;
import com.petgroom.demo.services.UserServices;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;

import com.petgroom.demo.models.User;

@Controller
@RequestMapping("/appointment")
public class AppointmentController {

    @Autowired
    private AppointmentRepo appointmentRepo;
    @Autowired
    private GroomerServices groomerService;
    @Autowired
    private GroomingServiceServices groomingServiceService;
    @Autowired
    private UserServices userService;

    @GetMapping
    public String showAppointmentForm(Model model) {
        List<GroomingService> groomingService = groomingServiceService.getAllGroomingServices();
        List<Groomer> groomer = groomerService.getAllGroomers();

        model.addAttribute("groomingService", groomingService);
        model.addAttribute("groomer", groomer);
        model.addAttribute("appointment", new Appointment());

        return "appointment";
    }

    @PostMapping
    public String createAppointment(@ModelAttribute("appointment") Appointment appointment, HttpSession session,
            HttpServletRequest request) {
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        UserDetails userDetails = (UserDetails) auth.getPrincipal();
        String email = userDetails.getUsername();
        Optional<User> loginUserOptional = userService.findByEmail(email);

        session.setAttribute("appointment", appointment);

        if (!loginUserOptional.isPresent()) {
            throw new IllegalStateException("System can not find the user.");
        }

        User loginUser = loginUserOptional.get();
        appointment.setUser(loginUser);

        Groomer selectedGroomer = groomerService.getGroomerById(appointment.getGroomer().getId());
        appointment.setGroomer(selectedGroomer);

        double basePrice;
        String petSize = appointment.getPetSize();
        if (petSize.equals("SMALL")) {
            basePrice = 9.9;
        } else if (petSize.equals("MEDIUM")) {
            basePrice = 14.9;
        } else if (petSize.equals("LARGE")) {
            basePrice = 19.9;
        } else {
            throw new IllegalArgumentException("Invalid pet size: " + petSize);
        }

        double multiplier;
        switch (selectedGroomer.getRanks()) {
            case 3:
                multiplier = 1.2;
                break;
            case 4:
                multiplier = 1.5;
                break;
            case 5:
                multiplier = 1.8;
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + selectedGroomer.getRanks());
        }

        List<Integer> selectedServiceIds = new ArrayList<>();
        if (request.getParameterValues("groomingServices") != null) {
            for (String serviceId : request.getParameterValues("groomingServices")) {
                selectedServiceIds.add(Integer.parseInt(serviceId));
            }
        }

        List<GroomingService> selectedServices = groomingServiceService.findByIds(selectedServiceIds);
        appointment.setGroomingServices(selectedServices);

        double servicePrice = 0;

        if (selectedServices != null && !selectedServices.isEmpty()) {
            for (GroomingService groomingService : selectedServices) {
                servicePrice += groomingService.getPrice();
            }
        }
        double calculatedPrice = basePrice * multiplier + servicePrice;
        appointment.setPrice(calculatedPrice);

        String appointmentDateStr = appointment.getAppointmentDateStr();
        if (appointmentDateStr == null) {
            
        } else {
            LocalDateTime parsedDate = LocalDateTime.parse(appointmentDateStr,
                    DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm"));
            appointment.setAppointmentDate(parsedDate);
        }

        if (appointment.getPaymentMethod().equals("ONLINE")) {
            // Online Pay
            session.setAttribute("appointment", appointment);
            return "redirect:/payment"; 
        } else {
            // Offline Pay
            appointmentRepo.save(appointment);
            return "index";
        }

    }
}
