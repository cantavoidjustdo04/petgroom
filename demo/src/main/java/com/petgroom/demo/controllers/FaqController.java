package com.petgroom.demo.controllers;

import java.util.List;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.beans.factory.annotation.Autowired;

import com.petgroom.demo.models.Faq;
import com.petgroom.demo.services.FaqServices;

@Controller
public class FaqController {

  @Autowired
  private final FaqServices faqService;

  public FaqController(FaqServices faqService) {
    this.faqService = faqService;
  }

  @GetMapping("/faq")
  public String showFaqPage(Model model) {
    List<Faq> faqs = faqService.getAllFaqs();
    if (!faqs.isEmpty()) {
      model.addAttribute("faqs", faqs);
    } else {
      System.out.println("is empty");
    }
    return "faq";
  }

}
