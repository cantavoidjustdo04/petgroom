package com.petgroom.demo.controllers;

import com.petgroom.demo.models.Appointment;
import com.petgroom.demo.models.CardInfo;
import com.petgroom.demo.repositories.AppointmentRepo;
import com.petgroom.demo.repositories.CardInfoRepo;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payment")
public class PaymentController {

    @Autowired
    private AppointmentRepo appointmentRepo;

    @Autowired
    private CardInfoRepo cardInfoRepo;

    @GetMapping
    public String showPaymentPage(Model model) {
        model.addAttribute("cardInfo", new CardInfo());
        return "payment";
    }

    @PostMapping
    public String completePayment(@ModelAttribute("cardInfo") CardInfo cardInfo, HttpSession session) {
        CardInfo savedCardInfo = cardInfoRepo.save(cardInfo);
        Appointment appointment = (Appointment) session.getAttribute("appointment");
        appointment.setCardInfo(savedCardInfo);
        appointmentRepo.save(appointment);
        session.removeAttribute("appointment"); 
        return "index";
    }
}
