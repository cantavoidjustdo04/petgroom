package com.petgroom.demo.security;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;


import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.petgroom.demo.models.User;

public class MyUserDetails implements UserDetails{

  private String userEmail;
  private String password;
  private boolean active;
  private List<GrantedAuthority> authorities;
  private User user;

  public MyUserDetails(){

  }

  public MyUserDetails(User user){
    this.userEmail = user.getEmail();
    this.password = user.getPassword();
    this.active = user.isActive();
    this.authorities = Arrays.asList(new SimpleGrantedAuthority("ROLE_" + user.getRole().toUpperCase()));
  }

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    return this.authorities;
  }
  
  @Override
  public String getUsername() {
    return this.userEmail;
  }

  @Override
  public String getPassword() {
    return this.password;
  }

  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  @Override
  public boolean isEnabled() {
    return this.active;
  }

  public User getUser() {
    return this.user;
}

  public void setUser(User user) {
    this.user = user;
  }
  
}
