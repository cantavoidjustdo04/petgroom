package com.petgroom.demo.config;

import java.util.Optional;

import org.springframework.stereotype.Service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;

import com.petgroom.demo.models.User;
import com.petgroom.demo.repositories.UserRepo;
import com.petgroom.demo.security.MyUserDetails;

@Service
public class MyUserDetailsService implements UserDetailsService{
  //Load the user from JPA
  @Autowired
  private UserRepo userRepo;

  @Override
  public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
    Optional<User> returneduser = userRepo.findByEmail(email);
    User user = returneduser.orElseThrow(() -> new UsernameNotFoundException("User not found"));
    return new MyUserDetails(user);
  }
  
}
