package com.petgroom.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.petgroom.demo.models.CardInfo;
import com.petgroom.demo.repositories.CardInfoRepo;



@Service
public class CardInfoServices {

    @Autowired
    private CardInfoRepo cardInfoRepo;

    // Create a new CardInfo
    public CardInfo save(CardInfo cardInfo) {
        return cardInfoRepo.save(cardInfo);
    }

    
}
