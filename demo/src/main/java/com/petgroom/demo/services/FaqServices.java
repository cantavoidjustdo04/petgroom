package com.petgroom.demo.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petgroom.demo.models.Faq;
import com.petgroom.demo.repositories.FaqRepo;
import java.util.List;

@Service
public class FaqServices {

  @Autowired
  private FaqRepo faqRepo;

    public Faq save(Faq faq) {
        return faqRepo.save(faq);
    }

    public List<Faq> getAllFaqs() {
        return faqRepo.findAllByOrderByCreatedAt();
    }
}
