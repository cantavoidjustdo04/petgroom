package com.petgroom.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petgroom.demo.models.Groomer;
import com.petgroom.demo.repositories.GroomerRepo;

@Service
public class GroomerServices {

    @Autowired
    private GroomerRepo groomerRepo;

    public Groomer saveGroomer(Groomer groomer) {
        return groomerRepo.save(groomer);
    }

    public List<Groomer> getAllGroomers() {
        return groomerRepo.findAll();
    }

    public Groomer getGroomerById(Integer id) {
        return groomerRepo.findById(id).orElse(null);
    }

    public void deleteGroomerById(Integer id) {
        groomerRepo.deleteById(id);
    }
}
