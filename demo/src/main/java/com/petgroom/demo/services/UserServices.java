package com.petgroom.demo.services;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.petgroom.demo.models.User;
import com.petgroom.demo.repositories.UserRepo;

@Service
public class UserServices {
  @Autowired
  private UserRepo userRepo;

  public User newUser(User user) {
    return userRepo.save(user);
  }

  public List<User> getUserList() {
    return userRepo.findAll();
  }

  public Optional<User> findByEmail(String email) {
    return userRepo.findByEmail(email);
  }

  public User getUser(String email) {
    Optional<User> user = userRepo.findByEmail(email);
    if (user.isPresent()) {
      return user.get();
    } else {
      // 사용자를 찾을 수 없는 경우 예외 처리
      throw new RuntimeException("User not found: " + email);
    }
  }

  public Map<String, Integer> getGenderRatioData() {
    Map<String, Integer> genderRatioData = new HashMap<>();
    // Fetch data and calculate gender ratio
    // ...

    int maleCount = userRepo.countByGender("Male");
    int femaleCount = userRepo.countByGender("Female");

    genderRatioData.put("Male", maleCount);
    genderRatioData.put("Female", femaleCount);

    return genderRatioData;
  }
}
