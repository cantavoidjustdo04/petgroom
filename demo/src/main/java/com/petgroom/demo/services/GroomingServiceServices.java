package com.petgroom.demo.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petgroom.demo.models.GroomingService;
import com.petgroom.demo.repositories.GroomingServiceRepo;

@Service
public class GroomingServiceServices {

  @Autowired
  private GroomingServiceRepo groomingServiceRepo;

  public GroomingService saveGroomingService(GroomingService groomingService) {
    return groomingServiceRepo.save(groomingService);
  }

  public List<GroomingService> getAllGroomingServices() {
    return groomingServiceRepo.findAll();
}

  public List<GroomingService> findByIds(List<Integer> ids) {
    return groomingServiceRepo.findAllById(ids);
  }

  public double getPrice(GroomingService groomingService) {
    return groomingService.getPrice();
  }
  
}