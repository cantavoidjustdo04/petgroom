package com.petgroom.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.petgroom.demo.models.Qna;
import com.petgroom.demo.repositories.QnaRepo;

@Service
public class QnaServices {

  @Autowired
  private QnaRepo qnaRepo;

  public Qna save(Qna qna) {
    return qnaRepo.save(qna);
  }

  public List<Qna> getAllQAs() {
    return qnaRepo.findAllByOrderByCreatedAt();
  }

  public Optional<Qna> findById(Integer id) {
    return qnaRepo.findById(id);
  }
}

