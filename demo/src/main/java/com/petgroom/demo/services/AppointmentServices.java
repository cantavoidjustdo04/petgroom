// AppointmentServices.java
package com.petgroom.demo.services;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.petgroom.demo.models.Appointment;
import com.petgroom.demo.repositories.AppointmentRepo;

@Service
public class AppointmentServices {
    @Autowired
    private AppointmentRepo appointmentRepo;

    public Appointment createAppointment(Appointment appointment) {
        return appointmentRepo.save(appointment);
    }

    public List<Appointment> getAllAppointments() {
        return appointmentRepo.findAll();
    }

    public Appointment getAppointmentById(int id) {
        return appointmentRepo.findById(id).orElse(null);
    }

    public Appointment updateAppointment(Appointment appointment) {
        return appointmentRepo.save(appointment);
    }

    public void deleteAppointment(int id) {
        appointmentRepo.deleteById(id);
    }

    public List<Integer> getMonthlyRevenueData() {
        List<Integer> monthlyRevenueData = new ArrayList<>();
        // Fetch data and calculate monthly revenue
        // ...

        LocalDate currentDate = LocalDate.now();
        int currentYear = currentDate.getYear();

        for (int month = 1; month <= 12; month++) {
            int monthlyRevenue = appointmentRepo.findTotalRevenueByMonth(currentYear, month);
            monthlyRevenueData.add(monthlyRevenue);
        }

        return monthlyRevenueData;
    }
}
